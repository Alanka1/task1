package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {
    public static void main(String args[]) {
        Connection connect = null;
        Statement state = null;
        Statement state2 = null;
        try {
            Class.forName("org.postgresql.Driver");
            connect = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "0912");
            connect.setAutoCommit(false);
            System.out.println("Opened database successfully");

            state = connect.createStatement();
            state2 = connect.createStatement();
            ResultSet res = state.executeQuery( "SELECT * FROM Student;" );
            while ( res.next() ) {
                Student student = new Student(res.getInt("id"), res.getString("name"), res.getString("phone"), res.getInt("groupid"));
                ResultSet res2 = state2.executeQuery("SELECT * FROM Groups WHERE id = "+ student.getGroupId() +";");
                Group group = new Group();
                while ( res2.next() ) {
                    group.setId(res2.getInt("id"));
                    group.setName(res2.getString("name"));
                }
                System.out.print(student);
                System.out.println(group);
            }
        } catch (Exception except) {
            except.printStackTrace();
            System.err.println(except.getClass().getName()+": "+except.getMessage());
            System.exit(0);
        } finally {

        }

    }
}
