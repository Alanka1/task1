CREATE TABLE student(
    id INT PRIMARY KEY,
    name VARCHAR(255),
    phone VARCHAR(255),
    groupId INT);
CREATE TABLE groups(
    id INT PRIMARY KEY,
    name VARCHAR(255));

INSERT INTO groups (id,name)
VALUES(11,'CS-2001'),
      (12,'CS-2002'),
      (13,'CS-2003'),
      (14,'CS-2004'),
      (15,'CS-2005'),
      (16,'CS-2006'),
      (17,'CS-2007'),
      (18,'CS-2008'),
      (19,'CS-2009'),
      (20,'CS-2010');
INSERT INTO student(id,name,phone,groupId)
VALUES(1,'Alanka','+77777777710',11),
      (2,'Serzhan','+77777777720',12),
      (3,'Tamer','+77777777730',13),
      (4,'Zhandos','+77777777740',14),
      (5,'Ivan','+77777777750',15),
      (6,'Aida','+77777777760',16),
      (7,'Asyltas','+77777777770',17),
      (8,'Damir','+77777777780',18),
      (9,'Almaz','+77777777790',19),
      (10,'Venera','+77777777700',20);



